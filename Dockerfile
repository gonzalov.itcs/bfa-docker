# Build Geth in a stock Go builder container
FROM golang:alpine as builder

RUN apk add --no-cache make gcc musl-dev linux-headers git

ADD ./go-ethereum/ /go-ethereum
RUN cd /go-ethereum && make geth


# Pull Geth into a second stage deploy alpine container
FROM alpine:latest


RUN apk add --no-cache ca-certificates
COPY --from=builder /go-ethereum/build/bin/geth /usr/local/bin/

VOLUME bfa-data:/home/bfa/network/node

COPY ./bfa/ /home/bfa/

EXPOSE 8545 8546 8547 30303 30303/udp
ENTRYPOINT geth --config /home/bfa/bfa.toml --gcmode=archive

