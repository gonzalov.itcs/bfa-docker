BFA docker
----------

* Cómo generar la imagen:
- Clonar este repositorio:
    `git clone https://gitlab.com/gonzalov.itcs/bfa-docker`
- Clonar go-ethereum dentro del repositorio anterior:
    `cd bfa-docker && git clone https://github.com/ethereum/go-ethereum.git`
- Generar la imagen:
    `docker build .`
